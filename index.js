// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var cors = require('cors') // add this line below it
var ParseServer = require('parse-server').ParseServer;
var path = require('path');


var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

var api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://lobeadmin:lobelive@ds023492-a0.mlab.com:23492,ds023492-a1.mlab.com:23492/lobemusic?replicaSet=rs-ds023492',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID || 'DlIbcE0Xiy5upPCDpZXneHfAD76lDBiGvwcT51ee',
  masterKey: process.env.MASTER_KEY || 'Xlbt9tGnTBhgywUn6h8RsqmrkckndKCqnLadyI7l', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'http://lobemusic.herokuapp.com/parse',  // Don't forget to change to https if needed
  facebookAppIds : [1283244075057524],
  oauth: { 
    facebook: { 
      appIds: [1283244075057524]
    },
    twitter: {
          consumer_key: "RNjmmzejGFfXOpZ5lgXH6l3Mc",
          consumer_secret: "tfzxQB8fqUO9UbCRJwodB4G0X8fIUbEf6mfptqFGwoHRg6DpE0"
    }
  },
  push: {
    android: {
      senderId: '123', // The Sender ID of GCM
      apiKey: '123' // The Server API Key of GCM
    },
    ios: {
      pfx: 'cloud/certificates/parsePushCertificates.p12', // The filename of private key and certificate in PFX or PKCS12 format from disk  
      bundleId: 'Ntambwa.Lobe', // The bundle identifier associate with your app
      production: false // Specifies which environment to connect to: Production (if true) or Sandbox
    }
  }
});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

var app = express();
app.use(cors()); 
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function(req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
