
var moment = require("moment");
var backgroundJobModule = require('cloud/modules/background_job_module.js');
var favoriteTrackModule = require('cloud/modules/favorite_track_module.js');
var recentTrackModule = require('cloud/modules/recent_track_module.js');
var pushModule = require('cloud/modules/push_module.js');
var followModule = require('cloud/modules/follow_module.js');
var activityModule = require('cloud/modules/activity_module.js');
var groupModule = require('cloud/modules/group_module.js');
var userModule = require('cloud/modules/user_module.js');
var suggestionAlgoModule = require('cloud/modules/suggestions_algo_module.js');
var lobeTrackModule = require('cloud/modules/lobe_track_module.js');
var playlistModule = require('cloud/modules/playlist_module.js');
var feedsModule = require('cloud/modules/feeds_module.js');
var sharedTrackModule = require('cloud/modules/most_shared_module.js');
var flagContentModule = require('cloud/modules/flag_content_module.js');


// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

// Gets online user
Parse.Cloud.define("getOnlineUsers", function(request, response) {
    var userQuery = new Parse.Query(Parse.User);
    var activeSince = moment().subtract("minutes", 10).toDate();
    userQuery.greaterThan("lastActive", activeSince);
    userQuery.find().then(function (users) {
        response.success(users);
    }, function (error) {
        response.error(error);
    });
});

//temporary fix for most shared module








